// console.log("Hello World!");

/* 1.A  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything. */

function addNumbers(num1, num2) {
	console.log ("Displayed sum of " + num1 + " and " + num2);
	console.log (num1 + num2);
}

/* -invoke and pass 2 arguments to the addition function */

addNumbers (5, 15);

/* 1.B Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.*/

function subtractNumbers (num1, num2) {
	console.log ("Displayed difference of " + num1 + " and " + num2);
	console.log (num1 - num2);
}


/* -invoke and pass 2 arguments to the subtraction function*/
subtractNumbers (20, 5);

/* 2.A Create a function which will be able to multiply two numbers.
	-Numbers must be provided as arguments.
	-Return the result of the multiplication.*/

function multiplyNumbers (num1, num2) {
	console.log("The product of " + num1 + " and " + num2 + ":");
	return num1 * num2;
}

/* Create a global variable called outside of the function called product.
	-This product variable should be able to receive and store the result of multiplication function.*/

let product = multiplyNumbers(50, 10);

/*Log the value of product variable in the console.*/
console.log(product);

/* 2.B Create a function which will be able to divide two numbers.
	-Numbers must be provided as arguments.
	-Return the result of the division.*/

function divideNumbers (num1, num2) {
	console.log("The quotient of " + num1 + " and " + num2 + ":");
	return num1 / num2;
}

/*Create a global variable called outside of the function called quotient.
	-This quotient variable should be able to receive and store the result of division function.*/

let quotient = divideNumbers(50, 10);

/*Log the value of quotient variable in the console.*/
console.log(quotient);

/* 3. Create a function which will be able to get total area of a circle from a provided 		radius.
		-a number should be provided as an argument.
		-look up the formula for calculating the area of a circle with a provided/given radius.
		-look up the use of the exponent operator.
		-you can save the value of the calculation in a variable.
		-return the result of the area calculation.*/


function getCircleArea(radius) {
	const pi = 3.1416;
	console.log("The result of getting the area of a circle with " + radius + " radius:");
	return (pi * radius * radius);
}

/* Create a global variable called outside of the function called circleArea.
	-This variable should be able to receive and store the result of the circle area calculation.*/

let circleArea = getCircleArea(15);

/*Log the value of the circleArea variable in the console.*/
console.log(circleArea);

/* 4.A Create a function which will be able to get total average of four numbers.
		-4 numbers should be provided as an argument.
		-look up the formula for calculating the average of numbers.
		-you can save the value of the calculation in a variable.
		-return the result of the average calculation.*/

function getTotalAverage(num1, num2, num3, num4) {
	console.log("The average of " + num1 + ", " + num2 + ", " + num3 + ", and " + num4 + ":");
	let sum = num1 + num2 + num3 + num4;
	return sum / 4 ;
}

/* 4.B Create a global variable called outside of the function called averageVar.
		-This variable should be able to receive and store the result of the average calculation
		-Log the value of the averageVar variable in the console.*/

let averageVar = getTotalAverage (20, 40, 60, 80);
console.log(averageVar)

/* 5.A Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
		-this function should take 2 numbers as an argument, your score and the total score.
		-First, get the percentage of your score against the total. You can look up the formula to get percentage.
		-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
		-return the value of the variable isPassed.
		-This function should return a boolean.*/

function checkIfPassed(score, totalScore) {
	let isPassed = true;
	let percentage = (score/totalScore) * 100;
	console.log("Is " + score + "/" + totalScore + " a passing score?");

	if (percentage>75) {
		return isPassed;
	} else {
		return !isPassed;
	}
}

/* 5.B Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.*/

let isPassingScore = checkIfPassed(38, 50);
console.log(isPassingScore);
